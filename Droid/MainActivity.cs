﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Graphics;
using Android.Content.Res;


namespace weightTracker.Droid
{
    [Activity(Label = "weightTracker", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        int count = 1;
        int count2 = 1;



        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

			// Lato-Regular.ttf
            var typeFace = Typeface.CreateFromAsset(Assets, "lato_hairline_italic.ttf");
            var latoBold = Typeface.CreateFromAsset(Assets, "lato_bold.ttf");


			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.myButton);
            Button btnLogin = FindViewById<Button>(Resource.Id.btnLogin);
            button.Typeface = typeFace;
            btnLogin.Typeface = latoBold;
            btnLogin.TextScaleX = 2;


            // Button btnLogin = FindViewById<Button>(Resource.Id.btnLogin);

            button.Click += delegate { button.Text = $"{count++} clicks!"; };

            btnLogin.Click += btnLoginClick;

        }



        private void btnLoginClick(object sender, EventArgs e)
        {
            
            count2 = count2 * 2;
            TextView txtStatus = FindViewById<TextView>(Resource.Id.txtStatus);
            txtStatus.Text = "Button Clicked";
        }
    }
}

