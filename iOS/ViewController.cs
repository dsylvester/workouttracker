﻿using System;

using UIKit;

namespace weightTracker.iOS
{
    public partial class ViewController : UIViewController
    {
        partial void addBtnAction(UIButton sender)
        {
			var title = string.Format("{0} clicks!", count * 2);
			//btnDavis.SetTitle(title, UIControlState.Normal);
            count++;
            outputBox.Text = title;
        }

        partial void Button_TouchUpInside(UIButton sender)
        {
            var title = string.Format("{0} clicks!", count * 2);
        }


        int count = 1;

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Code to start the Xamarin Test Cloud Agent
#if ENABLE_TEST_CLOUD
			Xamarin.Calabash.Start ();
#endif

            // Perform any additional setup after loading the view, typically from a nib.
            myCustomBtn.AccessibilityIdentifier = "myButton";
            myCustomBtn.TouchUpInside += delegate
            {
                var title = string.Format("{0} clicks!", count++);
                myCustomBtn.SetTitle(title, UIControlState.Normal);
            };


        }



        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.		
        }
    }
}
