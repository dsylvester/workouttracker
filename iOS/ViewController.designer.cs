// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace weightTracker.iOS
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        UIKit.UIButton Button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnDavis { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton myCustomBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField outputBox { get; set; }

        [Action ("addBtnAction:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void addBtnAction (UIKit.UIButton sender);

        [Action ("Button_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void Button_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (btnDavis != null) {
                btnDavis.Dispose ();
                btnDavis = null;
            }

            if (myCustomBtn != null) {
                myCustomBtn.Dispose ();
                myCustomBtn = null;
            }

            if (outputBox != null) {
                outputBox.Dispose ();
                outputBox = null;
            }
        }
    }
}